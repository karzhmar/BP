package xml.index.tpaUN;

import java.io.Serializable;

public class XMLTag implements Serializable {
    private String name;
    private int  prefixID;

    XMLTag(String name, int prefixID) {
        this.name = name;
        this.prefixID = prefixID;
    }

    int getPrefixID() {
        return prefixID;
    }

    String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "-> " + name + " " + Integer.toString(prefixID) + "\t";
    }
}
