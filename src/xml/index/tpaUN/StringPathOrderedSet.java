package xml.index.tpaUN;

import java.util.ArrayList;

public class StringPathOrderedSet {

    ArrayList<StringPath> stringPaths;

    StringPathOrderedSet() {
        this.stringPaths = new ArrayList<>();
    }

    @Override
    public String toString() {
        String str = "";
        for (StringPath p:this.stringPaths
             ) {
            str += p.toString() + "\n";
        }
        return str;
    }
}
