package xml.index.tpaUN;

import java.io.Serializable;
import java.util.*;

public class State implements Serializable {
    String label;
    HashSet<Transition> transitions;
    TreeMap<Integer, XMLTag> xmlTags;
    HashSet<State> consistsOfStates;

    State(XMLTag xmlT) {
        this.label = xmlT.getName();
        this.transitions = new HashSet<>();
        this.xmlTags = new TreeMap<>();
        this.xmlTags.put(xmlT.getPrefixID(), xmlT);
        this.consistsOfStates = new HashSet<>();
    }

    State() {
        this.label = null;
        this.transitions = new HashSet<>();
        this.xmlTags = new TreeMap<>();
        this.consistsOfStates = new HashSet<>();
    }

    State(ArrayList<XMLTag> xmlTags) {
        this.label = getLabelFromXMLTags(xmlTags);
        this.transitions = new HashSet<>();
        this.xmlTags = new TreeMap<>();
        for (XMLTag xmlTag : xmlTags)
            this.xmlTags.put(xmlTag.getPrefixID(), xmlTag);
        this.consistsOfStates = new HashSet<>();
    }

    private String getLabelFromXMLTags(ArrayList<XMLTag> xmlTags) {
        for (int i = 0; i < xmlTags.size() - 1; i ++)
            if (!xmlTags.get(i).getName().equals(xmlTags.get(i + 1).getName()))
                return "*";
        return xmlTags.get(0).getName();
    }

    ArrayList<XMLTag> getXmlTags() { return new ArrayList<>(xmlTags.values()); }

    Transition getTransitionByPhrase(String phrase, TransitionType transitionType){
        for (Transition t : this.transitions) {
            if (t.containsPhrase(phrase) && t.transitionType.equals(transitionType)) return t;
        }
        return null;
    }

    // add a Slash transition by default
    void addTransition(State to, String phrase) {
        addTransition(to, phrase, TransitionType.Slash);
    }

    void addTransition(State to, String phrase, TransitionType transitionType) {
        // add the transition
        this.transitions.add(new Transition(this, to, phrase, transitionType));
    }

    void addWildcardTransition(State to) {
        addWildcardTransition(to, TransitionType.Slash);
    }

    void addWildcardTransition(State to, TransitionType transitionType) {
        // add the transition
        this.transitions.add(new Transition(this, to, "*", transitionType));
    }

    Boolean hasTransitionOn(String phrase, TransitionType transitionType) {
        for (Transition transition : this.transitions)
            if (transition.phrase.equals(phrase) && transition.transitionType.equals(transitionType)) return true;
        return false;
    }

    void copyTransitions(State from) {
        this.transitions.addAll(from.transitions);
    }

    void copyTransitionsOfType(State from, TransitionType transitionType) {
        for (Transition transition : from.transitions)
            if (transition.transitionType == transitionType)
                this.transitions.add(transition);
    }

    void addModifyTransition(Transition newTransition) {
        Transition transition = getTransitionByPhrase(newTransition.phrase, newTransition.transitionType);
        State to = copyStateNoTransitions(newTransition.to);

        if (transition == null) {
            addTransition(to, newTransition.phrase, newTransition.transitionType);
            to.consistsOfStates.add(newTransition.to);
        } else {
            transition.to.addXMLTags(new ArrayList<>(newTransition.to.xmlTags.values()));
            transition.to.consistsOfStates.add(newTransition.to);
        }
    }

    int getMinXMLTag() { return xmlTags.firstKey(); }

    Boolean inCollection(Collection<State> collection) {
        for (State state : collection) {
            Set<Integer> thisIDs = getXMLTagsIDs(this.xmlTags);
            Set<Integer> stateIDs = getXMLTagsIDs(state.xmlTags);
            if (stateIDs.containsAll(thisIDs) && thisIDs.containsAll(stateIDs)) return true;
        }
        return false;
    }

    State getFromCollection(Collection<State> collection) {
        for (State state : collection) {
            Set<Integer> thisIDs = getXMLTagsIDs(this.xmlTags);
            Set<Integer> stateIDs = getXMLTagsIDs(state.xmlTags);
            if (stateIDs.containsAll(thisIDs) && thisIDs.containsAll(stateIDs)) return state;
        }
        return null;
    }

    Set<Integer> getXMLTagsIDs() { return this.xmlTags.keySet(); }

    Set<Integer> getXMLTagsIDs(TreeMap<Integer, XMLTag> xmlTags) { return xmlTags.keySet(); }

    private State copyStateNoTransitions(State state) {
        State newState = new State();
        newState.label = state.label;
        for (XMLTag xmlTag : state.xmlTags.values()) {
            XMLTag newXMLTag = new XMLTag(xmlTag.getName(), xmlTag.getPrefixID());
            newState.addXMLTag(newXMLTag);
        }
        return newState;
    }

    private void addXMLTags(ArrayList<XMLTag> newXMLTags) {
        for (XMLTag xmlTag : newXMLTags) {
            if (xmlTags.containsKey(xmlTag.getPrefixID())) continue;
            this.xmlTags.put(xmlTag.getPrefixID(), xmlTag);
        }
    }

    private void addXMLTag(XMLTag xmlTag) {
        if (xmlTags.containsKey(xmlTag.getPrefixID())) return;
        this.xmlTags.put(xmlTag.getPrefixID(), xmlTag);
    }

    @Override
    public String toString() {
        String tmp = "( ";
        for (XMLTag tag : this.xmlTags.values()) {
            tmp += tag.getPrefixID()+" ";
        }
        tmp += ")" ;
        return  tmp;
    }
}
