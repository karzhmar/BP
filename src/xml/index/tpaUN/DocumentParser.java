package xml.index.tpaUN;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;

class DocumentParser {

    private static class stringPathVars {
        static int pid = 0;
        private static boolean xmlTagAddedinLastStep;

        private static void incrementID() {
            pid++;
        }
    }

    static StringPathOrderedSet getStringPathsSet(File xmlDocumnet){

        StringPath currentStringPath = new StringPath();
        StringPathOrderedSet stringPathOrderedSet = new StringPathOrderedSet();

        try {

            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            DefaultHandler defaultHandler = new DefaultHandler(){

                @Override
                public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
                    super.startElement(uri, localName, qName, attributes);
                    stringPathVars.incrementID();

                    //creating StringPaths
                    currentStringPath.addXmlTag(new XMLTag(qName,stringPathVars.pid));
                    stringPathVars.xmlTagAddedinLastStep = true;
                }

                @Override
                public void characters(char[] ch, int start, int length) throws SAXException {
                }

                @Override
                public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
                    super.ignorableWhitespace(ch, start, length);
                }

                @Override
                public void endElement(String uri, String localName, String qName) throws SAXException {
                    if (stringPathVars.xmlTagAddedinLastStep){
                        stringPathOrderedSet.stringPaths.add(currentStringPath.clone());
                    }
                    currentStringPath.removeLastXmlTag();
                    stringPathVars.xmlTagAddedinLastStep = false;
                }
            };


            saxParser.parse(xmlDocumnet, defaultHandler);

        }catch (ParserConfigurationException | SAXException | IOException e){
            e.printStackTrace();
        }
        return stringPathOrderedSet;
    }
}
