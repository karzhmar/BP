package xml.index.tpaUN;

import java.io.Serializable;

public class Transition implements Serializable {
    State from;
    State to;
    String phrase;
    TransitionType transitionType;

    // create a Slash transition by default
    Transition(State from, State to, String phrase) {
        this(from, to, phrase, TransitionType.Slash);
    }

    Transition(State from, State to, String phrase, TransitionType transitionType) {
        this.from = from;
        this.to = to;
        this.phrase = phrase;
        this.transitionType = transitionType;
    }

    boolean containsPhrase(String p){
        return phrase.equalsIgnoreCase(p);
    }

    @Override
    public String toString() {
        if (this.transitionType == TransitionType.Slash)
            return "/" + this.phrase;
        else
            return  "//" + this.phrase;
    }
}
