package xml.index.tpaUN;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.TreeMap;

public class Automaton implements Serializable {
    State firstState;
    ArrayList<State> states;
    AutomatonType automatonType;

    Automaton(State firstState, AutomatonType automatonType) {
        this.firstState = firstState;
        states = new ArrayList<>();
        states.add(firstState);
        this.automatonType = automatonType;
    }

    State getNextState(State state) {
        int ind = this.states.indexOf(state);
        return this.states.get(ind + 1);
    }

    State getState(ArrayList<XMLTag> xmlTags) {
        TreeMap<Integer, XMLTag> newXMLTags = new TreeMap<>();
        for (XMLTag xmlTag : xmlTags)
            newXMLTags.put(xmlTag.getPrefixID(), xmlTag);
        for (State state : this.states) {
            if (state.getXMLTagsIDs().containsAll(state.getXMLTagsIDs(newXMLTags)) &&
                    state.getXMLTagsIDs(newXMLTags).containsAll(state.getXMLTagsIDs()))
            return state;
        }
        return null;
    }

    State getStateMinID(int minID) {
        for (State state : this.states)
            if (state.xmlTags.size() != 0 && state.getMinXMLTag() == minID)
                return state;
        return null;
    }

    HashSet<State> getStatesUniqueID(ArrayList<XMLTag> xmlTags) {
        HashSet<State> statesUniqueID = new HashSet<>();
        for (State state : this.states) {
            if (state.xmlTags.size() == 1) {
                for (XMLTag xmlTag : xmlTags) {
                    if (state.xmlTags.firstEntry().getValue().getPrefixID() == xmlTag.getPrefixID())
                        statesUniqueID.add(state);
                }
            }
        }
        return statesUniqueID;
    }

    ArrayList<XMLTag> resolveQuery(String path) throws Exception {

        StringPath stringPath = new StringPath();
        String[] strings = path.split(("//"));
        State currentState = this.firstState;

        for (String str:strings) {
            if (!str.isEmpty()) {
                if (!str.contains("/")) {
                    stringPath.addXmlTag(new XMLTag(str, 2));
                } else {
                    String[] s = str.split("/");
                    if (!s[0].isEmpty()) {
                        stringPath.addXmlTag(new XMLTag(s[0], 2));
                    }
                    for (int i = 1; i < s.length; i++) {
                        stringPath.addXmlTag(new XMLTag(s[i], 1));
                    }
                }
            }
        }

        for (XMLTag tag : stringPath.getXmlTags()) {
            if (tag.getPrefixID() == 1) {
                if (currentState.getTransitionByPhrase(tag.getName(), TransitionType.Slash) != null)
                    currentState = currentState.getTransitionByPhrase(tag.getName(), TransitionType.Slash).to;
                else
                    throw new Exception("No satisfying elements");
            } else {
                if (currentState.getTransitionByPhrase(tag.getName(), TransitionType.DoubleSlash) != null)
                    currentState = currentState.getTransitionByPhrase(tag.getName(), TransitionType.DoubleSlash).to;
                else
                    throw new Exception("No satisfying elements");

            }
        }

        return currentState.getXmlTags();
    }

    void serialize(String path) throws IOException {
        //FileOutputStream fout = new FileOutputStream(path);
        //ObjectOutputStream oos = new ObjectOutputStream(fout);
        //oos.writeObject(this);
    }

    @Override
    public String toString() {
        return this.firstState.toString();
    }
}
