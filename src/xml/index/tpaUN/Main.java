package xml.index.tpaUN;

import java.io.File;
import java.util.ArrayList;

public class Main {

    static String path;
    static String query;

    public static void main(String[] args) {

        //String path = "src/sampleXMLs/mine.xml";

        //String path = "src/sampleXMLs/XMark-f0.xml";
        //String path = "src/XMark-f0.001.xml";
        String path = "src/XMark-f0.005.xml";
        //String path = "src/sampleXMLs/XMark-f0.01.xml";
        //String path = "src/sampleXMLs/XMark-f0.1.xml"; //out of memory :C
        //String path = "src/sampleXMLs/XMark-f0.5.xml"; //out of memory :C
        /**
         * *********************Test XMark queries******************************
         */
        String query1 = "/site/*";
        String query2 = "/site/people/*/name";
        String query3 = "/site/regions/*/item/description/parlist/*/text/emph";
        String query4 = "//person//*";
        String query5 = "//regions//*//date";
        String query6 = "//site//regions//*//description//*//text//emph";
        String query7 = "/*//open_auction";
        String query8 = "//*/person/*";
        String query9 = "//regions/europe//item//*/listitem//text/*";

        //if (args.length != 1)
          //  return;
        //String path = args[0];

        Automaton automaton = AutomatonFactory.getTPAUnknownNodes(new File(path));

        Integer tr = 0;
        for (State state : automaton.states)
            tr += state.transitions.size();

        System.out.println(automaton.states.size() + " " + tr);

        try {
            long startTime = System.nanoTime();
            ArrayList<XMLTag> result1 = automaton.resolveQuery(query1);
            long stopTime = System.nanoTime();
            System.out.println( result1.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result2 = automaton.resolveQuery(query2);
            stopTime = System.nanoTime();
            System.out.println( result2.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result3 = automaton.resolveQuery(query3);
            stopTime = System.nanoTime();
            System.out.println( result3.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result4 = automaton.resolveQuery(query4);
            stopTime = System.nanoTime();
            System.out.println( result4.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result5 = automaton.resolveQuery(query5);
            stopTime = System.nanoTime();
            System.out.println( result5.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result6 = automaton.resolveQuery(query6);
            stopTime = System.nanoTime();
            System.out.println( result6.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result7 = automaton.resolveQuery(query7);
            stopTime = System.nanoTime();
            System.out.println( result7.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result8 = automaton.resolveQuery(query8);
            stopTime = System.nanoTime();
            System.out.println( result8.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));

            startTime = System.nanoTime();
            ArrayList<XMLTag> result9 = automaton.resolveQuery(query9);
            stopTime = System.nanoTime();
            System.out.println( result9.size() + " QueryEvalTime: " + ((stopTime - startTime) / (double) 1000000));
        } catch(Exception e) {
            System.out.println(e.getMessage());
        }

    }
}
