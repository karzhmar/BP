package xml.index.tpaUN;

import java.util.ArrayList;

public class StringPath {

    private ArrayList<XMLTag> xmlTags;

    StringPath() {
        this.xmlTags = new ArrayList<>();
    }

    void addXmlTag(XMLTag tag){
        this.xmlTags.add(tag);
    }

    void removeLastXmlTag(){
        this.xmlTags.remove(this.xmlTags.size() - 1);
    }

    public StringPath clone(){
        StringPath tmp = new StringPath();
        for (XMLTag t:this.xmlTags) {
            tmp.addXmlTag(t);
        }
        return tmp;
    }

    ArrayList<XMLTag> getXmlTags() {
        return xmlTags;
    }

    @Override
    public String toString() {
        String str = "";
        for (XMLTag t:this.xmlTags) {
            str += t.toString();
        }
        return str;
    }
}
