package xml.index.tpaUN;

public enum AutomatonType {
    TreeStringPaths,
    TreeStringPathsSubsequences,
    TreePaths
}
