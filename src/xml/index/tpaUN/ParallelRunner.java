package xml.index.tpaUN;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class ParallelRunner extends Thread {
    private ArrayList<Automaton> automata;
    Automaton automaton;
    private AutomatonType automatonType;
    Boolean imDone;

    ParallelRunner(ArrayList<Automaton> automata, AutomatonType automatonType) {
        this.automata = automata;
        this.automatonType = automatonType;
        imDone = false;
    }

    @Override
    public void run() {
        if (automata.size() == 1) automaton = automata.get(0);
        else automaton = parallelRun(automata, automatonType);
        imDone = true;
    }

    static Automaton parallelRun(List<Automaton> stringPathsAutomata, AutomatonType automatonType) {
        // initialization
        List<Automaton> automata = new ArrayList<>(stringPathsAutomata);
        xml.index.tpaUN.State state0 = new xml.index.tpaUN.State();
        Automaton automaton = new Automaton(state0, automatonType);
        for (Automaton stringPathAutomaton : automata) {
            state0.consistsOfStates.add(stringPathAutomaton.firstState);
        }
        LinkedList<xml.index.tpaUN.State> toDoStates = new LinkedList<>();
        toDoStates.addLast(state0);

        mainCycleParallelRun(toDoStates, automaton);
        return automaton;
    }

    static void mainCycleParallelRun(LinkedList<xml.index.tpaUN.State> toDoStates, Automaton automaton) {
        while (! toDoStates.isEmpty()) {
            xml.index.tpaUN.State state = toDoStates.pop();

            for (xml.index.tpaUN.State consistsOfState : state.consistsOfStates)
                for (Transition transition : consistsOfState.transitions)
                    state.addModifyTransition(transition);

            for (Transition transition : state.transitions) {
                xml.index.tpaUN.State transitionTo = transition.to;
                if (transitionTo.inCollection(toDoStates)) {
                    xml.index.tpaUN.State sameState = transitionTo.getFromCollection(toDoStates);
                    if (!sameState.consistsOfStates.containsAll(transitionTo.consistsOfStates))
                        sameState.consistsOfStates.addAll(transitionTo.consistsOfStates);
                    transition.to = sameState;
                }
                else {
                    if (transitionTo.inCollection(automaton.states)) {
                        xml.index.tpaUN.State sameState = transitionTo.getFromCollection(automaton.states);
                        if (!sameState.consistsOfStates.containsAll(transitionTo.consistsOfStates)) {
                            sameState.consistsOfStates.addAll(transitionTo.consistsOfStates);
                            toDoStates.addLast(sameState);
                        }
                        transition.to = sameState;
                    }
                    else {
                        toDoStates.addLast(transitionTo);
                        automaton.states.add(transitionTo);
                    }
                }
            }
        }
    }
}
