package xml.index.tpaUN;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AutomatonFactory {

    //=====================================================================================
    // SELECTING UNKNOWN NODES PART

    public static Automaton getTSPAUnknownNodes(File xmlDocument) {
        String str = "src/" + xmlDocument.getName() + "-TSPAUnknownNodes.ser";
        File f = new File(str);
        if (f.exists() && !f.isDirectory()) {
            return deserialize(str);
        }
        Automaton tspaUN = buildTSPAUnknownNodes(xmlDocument);
        try {
            tspaUN.serialize("src/" + xmlDocument.getName() + "-TSPAUnknownNodes.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tspaUN;
    }

    public static Automaton getTSPSAUnknownNodes(File xmlDocument) {
        String str = "src/" + xmlDocument.getName() + "-TSPSAUnknownNodes.ser";
        File f = new File(str);
        if (f.exists() && !f.isDirectory()) {
            return deserialize(str);
        }
        Automaton tspsaUN = buildTSPSAUnknownNodes(xmlDocument);
        try {
            tspsaUN.serialize("src/" + xmlDocument.getName() + "-TSPSAUnknownNodes.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tspsaUN;
    }

    public static Automaton getTPAUnknownNodes(File xmlDocument) {
        String str = "out/" + xmlDocument.getName() + "-TPAUnknownNodes.ser";
        File f = new File(str);
        if (f.exists() && !f.isDirectory()) {
            return deserialize(str);
        }

        System.out.println("Building index...");
        long startTime = System.currentTimeMillis();

        Automaton tpaUN = buildTPAUnknownNodes(xmlDocument);

        long stopTime = System.currentTimeMillis();
        System.out.println( "Index built in:  " + (int) (stopTime - startTime) + " ms");

        try {
            tpaUN.serialize("out/" + xmlDocument.getName() + "-TPAUnknownNodes.ser");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return tpaUN;
    }

    //======================== DESERIALIZATION PART =====================================

    private static Automaton deserialize(String path) {
        System.out.println("Deserializing...");
        ObjectInputStream objectInputStream;
        Automaton automaton = null;
        try {
            objectInputStream = new ObjectInputStream(new FileInputStream(path));
            automaton = (Automaton) objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(AutomatonFactory.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.println("Deserialized");
        return automaton;
    }

    //======================= BUILDING AUTOMATA PART ======================================

    private static Automaton buildTSPAUnknownNodes(File xmlDocument) {
        StringPathOrderedSet stringPathSet = DocumentParser.getStringPathsSet(xmlDocument);

        List<Automaton> stringPathsAutomata = new ArrayList<>();
        for (StringPath stringPath : stringPathSet.stringPaths)
            stringPathsAutomata.add(buildTSPAUNStringPath(stringPath));

        //return parallelRun(stringPathsAutomata); // sequence parallel run of automata
        return parallelParallelRun(stringPathsAutomata, AutomatonType.TreeStringPaths); // parallel parallel run of automata
    }

    private static Automaton buildTSPSAUnknownNodes(File xmlDocument) {
        StringPathOrderedSet stringPathSet = DocumentParser.getStringPathsSet(xmlDocument);

        List<Automaton> stringPathsAutomata = new ArrayList<>();
        for (StringPath stringPath : stringPathSet.stringPaths)
            stringPathsAutomata.add(buildTSPSAUNStringPath(stringPath));

        //return parallelRun(stringPathsAutomata); // sequence parallel run of automata
        return parallelParallelRun(stringPathsAutomata, AutomatonType.TreeStringPathsSubsequences); // parallel parallel run of automata
    }

    private static Automaton buildTPAUnknownNodes(File xmlDocument) {
        StringPathOrderedSet stringPathSet = DocumentParser.getStringPathsSet(xmlDocument);

        List<Automaton> stringPathsAutomata = new ArrayList<>();
        for (StringPath stringPath : stringPathSet.stringPaths) {
            Automaton a = buildTPAUNStringPath(stringPath);
            stringPathsAutomata.add(a);
            System.out.println(stringPath.getXmlTags().size() + " " + a.states.size());
        }

        //return parallelRun(stringPathsAutomata, xml.index.tpaUN.AutomatonType.TreePaths); // sequence parallel run of automata
        return parallelParallelRun(stringPathsAutomata, AutomatonType.TreePaths); // parallel parallel run of automata
    }

    //=========================== HELP FUNCTIONS ==========================================

    private static Automaton parallelParallelRun(List<Automaton> stringPathsAutomata, AutomatonType automatonType) {
        int num = stringPathsAutomata.size() / 20;
        if (num == 0) num = stringPathsAutomata.size();
        return parallelParallelRun(stringPathsAutomata, automatonType, num);
    }

    private static Automaton parallelParallelRun(List<Automaton> stringPathsAutomata, AutomatonType automatonType, int num) {
        ArrayList<ParallelRunner> runners = new ArrayList<>();
        LinkedList<Automaton> automataToRun = new LinkedList<>(stringPathsAutomata);

        while (automataToRun.size() != 1) {
            while (automataToRun.size() > 1) {
                ArrayList<Automaton> automata = new ArrayList<>();
                int numTo = num;
                if (automataToRun.size() < num) numTo = automataToRun.size();
                for (int i = 0; i < numTo; i ++) {
                    automata.add(automataToRun.pop());
                }

                ParallelRunner runner = new ParallelRunner(automata, automatonType);
                runners.add(runner);
                runner.run();
            }

            ArrayList<ParallelRunner> runnersToDelete = new ArrayList<>();
            while (runnersToDelete.size() < 2) {
                if (runners.size() == 1 && runners.get(0).imDone) {
                    runnersToDelete.add(runners.get(0));
                    automataToRun.addLast(runners.get(0).automaton);
                    break;

                }
                for (ParallelRunner runner : runners) {
                    if (runner.imDone) {
                        runnersToDelete.add(runner);
                        automataToRun.addLast(runner.automaton);
                    }
                }
            }

            runners.removeAll(runnersToDelete);
        }
        return automataToRun.get(0);
    }

    private static Automaton parallelRun(List<Automaton> stringPathsAutomata) {
        return ParallelRunner.parallelRun(stringPathsAutomata, stringPathsAutomata.get(0).automatonType);
    }

    private static Automaton buildTSPAUNStringPath(StringPath stringPath) {
        // initialize the initial state and an empty automaton
        State state0 = new State();
        Automaton automaton = new Automaton(state0, AutomatonType.TreeStringPaths);

        // for each XML tag in the string path add a state with this XML tag (empty transitions) to the automaton
        for (XMLTag xmlTag : stringPath.getXmlTags())
            automaton.states.add(new State(xmlTag));

        // add transitions to all states of the automaton
        ArrayList<State> states = automaton.states;
        for (int i = 0; i < states.size() - 1; i ++) {
            State state = states.get(i);
            State to = automaton.getNextState(state);
            state.addTransition(to, to.label);
            state.addWildcardTransition(to);
        }

        return automaton;
    }

    private static Automaton buildTSPSAUNStringPath(StringPath stringPath) {
        // initialize the initial state and an empty automaton
        State state0 = new State();
        Automaton automaton = new Automaton(state0, AutomatonType.TreeStringPathsSubsequences);
        TransitionType transitionType = TransitionType.DoubleSlash;

        // COMPUTE SETS OF OCCURRENCES FOR EACH LABEL
        Map<String, ArrayList<XMLTag>> occurrencesSets= new LinkedHashMap<String, ArrayList<XMLTag>>();
        ArrayList<XMLTag> xmlTags = new ArrayList<>(stringPath.getXmlTags());
        for (XMLTag xmlTag : xmlTags) {
            if (occurrencesSets.containsKey(xmlTag.getName())) {
                ArrayList<XMLTag> labelXMLTags = occurrencesSets.get(xmlTag.getName());
                labelXMLTags.add(xmlTag);
            } else {
                ArrayList<XMLTag> XMLTagsList = new ArrayList<>();
                XMLTagsList.add(xmlTag);
                occurrencesSets.put(xmlTag.getName(),XMLTagsList);
            }
        }

        // BUILD THE "BACKBONE"
        // for each XML tag in the string path add a state with this XML tag (empty transitions) to the automaton
        for (XMLTag xmlTag : stringPath.getXmlTags()) {
            String label = xmlTag.getName();
            automaton.states.add(new State(new ArrayList<>(occurrencesSets.get(label))));
            occurrencesSets.get(label).remove(0);
        }

        // add transitions to all states of the automaton
        List<State> states = automaton.states;
        for (int i = 0; i < states.size() - 1; i ++) {
            State state = states.get(i);
            State to = automaton.getNextState(state);
            state.addTransition(to, to.label, transitionType);
        }

        // ADD "ADDITIONAL TRANSITIONS"
        for (int i = 0; i < states.size() - 1; i ++) {
            for (int j = i + 1; j < states.size(); j ++) {
                State from = states.get(i);
                State to = states.get(j);
                String phrase = to.label;
                if (from.hasTransitionOn(phrase, transitionType)) continue;
                from.addTransition(to, phrase, transitionType);
            }
        }

        // ADD "WILDCARD STATES and TRANSITIONS"
        int stringPathLength = stringPath.getXmlTags().size();
        State prevState = null;
        for (int i = 1; i <= stringPathLength; i ++) {
            // add or select a state with all the XML tags needed
            State state = automaton.getState(xmlTags);
            if (state == null) {
                state = new State(new ArrayList<>(xmlTags));
                automaton.states.add(state);
            }

            // add a "wildcard transition"
            states.get(i - 1).addWildcardTransition(state, transitionType);

            // copy transitions from "usual state" to "wildcard state"
            if (i > 1) {
                prevState.copyTransitions(states.get(i - 1));
            }

            // save newly created (or modified) state
            prevState = state;

            xmlTags.remove(0);
        }

        return automaton;
    }

    private static Automaton buildTPAUNStringPath(StringPath stringPath) {
        // build TSPA* and TSPSA* for the string path
        Automaton TSPAUNStringPath = buildTSPAUNStringPath(stringPath);
        Automaton TSPSAUNStringPath = buildTSPSAUNStringPath(stringPath);

        // run constructed automata in parallel
        List<Automaton> stringPathAutomataList = new ArrayList<>();
        stringPathAutomataList.add(TSPAUNStringPath);
        stringPathAutomataList.add(TSPSAUNStringPath);
        Automaton automaton = ParallelRunner.parallelRun(stringPathAutomataList, AutomatonType.TreePaths);

        // add "missing" transitions and states
        LinkedList<State> statesToCheck = new LinkedList<>(automaton.states);
        while (! statesToCheck.isEmpty()) {
            State state = statesToCheck.pop();
            if (state.xmlTags.size() == 0) continue;
            state.consistsOfStates.clear();
            if (state.xmlTags.size() == 1) {
                State copyFrom = automaton.getStateMinID(state.xmlTags.firstEntry().getValue().getPrefixID());
                state.copyTransitionsOfType(copyFrom, TransitionType.DoubleSlash);
            }
            else {
                state.consistsOfStates = automaton.getStatesUniqueID(new ArrayList<>(state.xmlTags.values()));
                LinkedList<State> toDoStates = new LinkedList<>();
                toDoStates.addLast(state);
                int statesNumber = automaton.states.size();
                ParallelRunner.mainCycleParallelRun(toDoStates, automaton);
                if (statesNumber < automaton.states.size())
                    for (int i = statesNumber; i < automaton.states.size(); i ++)
                        statesToCheck.addLast(automaton.states.get(i));
            }
        }

        return automaton;
    }
}
